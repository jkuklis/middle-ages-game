/** @file
    Implementation of game enginge.

 */

#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>

#include "engine.h"
#include "container.h"

int min (int a, int b) {
    return (a < b ? a : b);
}

/**
 * Structure for holding game state data.
 */
typedef struct def_state {
    int size; /**< Size of board. */
    int turns; /**< Number of turns. */
    int current_turn; /**< Current turn. */
    int current_player; /**< Current player. */
    int number_of_players; /**< Number of players. */
    int init_pos[4]; /**< Initial positions of players' kings. */
    bool in_game[3]; /**< Determines, if player is initialized and has a king. */
    bool init_done; /**< Determines, if both initializations occurred. */
    container units; /**< Holds units data. */
} game_state;

/**
 * Actual game data.
 */
game_state state;

void start_game() {
    state.size = -1;
    state.turns = -1;
    state.current_turn = 1;
    state.current_player = 1;
    state.number_of_players = 2;
    state.init_pos[0] = state.init_pos[1] = state.init_pos[2] = state.init_pos[3] = -1;
    state.in_game[0] = 1;
    state.in_game[1] = state.in_game[2] = 0;
    state.init_done = 0;
}

void end_game() {
    free_container(&(state.units));
}

void print_topleft() {
    for (int i = 1; i <= min(state.size, 10); ++i) {
        for (int j = 1; j <= min(state.size, 10); ++j) {
            char to_print;
            unit unit_at = read_unit(state.units, j, i);

            switch (unit_at.type) {
                case NONE:
                    to_print = '.';
                    break;
                case KING:
                    to_print = 'k';
                    break;
                case KNIGHT:
                    to_print = 'r';
                    break;
                case PEASANT:
                    to_print = 'c';
                    break;
                default:
                    assert(false);
                    break;
            }
            if (unit_at.owner == 1)
                to_print = (char)toupper(to_print);

            printf("%c", to_print);
        }
        printf("\n");
    }
    printf("\n");
}

/**
 * Checks initial positions.
 * @param[in] x1 Number of column, first king.
 * @param[in] y1 Number of row, first king.
 * @param[in] x2 Number of column, second king.
 * @param[in] y2 Number of row, second king.
 * @return True if correct.
 */
bool init_pos_check (int x1, int y1, int x2, int y2) {
    if (state.init_pos[0] == -1) {
        state.init_pos[0] = x1;
        state.init_pos[1] = y1;
        state.init_pos[2] = x2;
        state.init_pos[3] = y2;
        return true;
    }

    bool correct_first = (state.init_pos[0] == x1 && state.init_pos[1] == y1);
    bool correct_second = (state.init_pos[2] == x2 && state.init_pos[3] == y2);

    return (correct_first && correct_second);
}

/**
 * Checks positions.
 * Checks if positions demanded are on the board.
 * @param[in] x1 Number of column, first position.
 * @param[in] y1 Number of row, first position.
 * @param[in] x2 Number of column, second position.
 * @param[in] y2 Number of row, second position.
 * @param[in] init Determines, if the command to execute is INIT.
 * @return True if correct.
 */
bool size_check (int x1, int y1, int x2, int y2, bool init) {
    int to_sub = (init ? 3 : 0);

    bool incorrect_first = (state.size - to_sub < x1 || state.size < y1);
    bool incorrect_second = (state.size - to_sub < x2 || state.size < y2);

    return !(incorrect_first || incorrect_second);
}

/**
 * Checks command INIT.
 * @param[in] size Size.
 * @param[in] turns Turns before game end.
 * @param[in] player Player.
 * @param[in] x1 Number of column, first position.
 * @param[in] y1 Number of row, first position.
 * @param[in] x2 Number of column, second position.
 * @param[in] y2 Number of row, second position.
 * @return True if correct.
 */
bool init_check (int size, int turns, int player, int x1, int y1, int x2, int y2) {
    bool incorrect_size = (state.size != -1 && state.size != size);
    state.size = size;

    bool incorrect_turns = (state.turns != -1 && state.turns != turns);
    state.turns = turns;

    bool incorrect_player = (state.in_game[player] != false);

    if (incorrect_size || incorrect_turns || incorrect_player
        || !size_check(x1, y1, x2, y2, true) || !init_pos_check(x1, y1, x2, y2))
        return false;

    return true;
}

message init(int size, int turns, int player, int x1, int y1, int x2, int y2) {
    if (!init_check(size, turns, player, x1, y1, x2, y2))
        return ERROR;

    if (!state.in_game[1] && !state.in_game[2]) {
        bool b1 = put_unit(&(state.units), x1, y1, make_unit(KING, 1, 0));
        bool b2 = put_unit(&(state.units), x1+1, y1, make_unit(PEASANT, 1, 0));
        bool b3 = put_unit(&(state.units), x1+2, y1, make_unit(KNIGHT, 1, 0));
        bool b4 = put_unit(&(state.units), x1+3, y1, make_unit(KNIGHT, 1, 0));
        bool b5 = put_unit(&(state.units), x2, y2, make_unit(KING, 2, 0));
        bool b6 = put_unit(&(state.units), x2+1, y2, make_unit(PEASANT, 2, 0));
        bool b7 = put_unit(&(state.units), x2+2, y2, make_unit(KNIGHT, 2, 0));
        bool b8 = put_unit(&(state.units), x2+3, y2, make_unit(KNIGHT, 2, 0));
        assert(b1 && b2 && b3 && b4 && b5 && b6 && b7 && b8);
    }

    state.in_game[player] = true;

    if (state.in_game[1] && state.in_game[2])
        state.init_done = true;

    return CONTINUE;
}

/**
 * Returns a winner of a fight.
 * @param[in] first First unit.
 * @param[in] second Second unit.
 * @return The winner of a fight.
 */
unit fight (unit first, unit second) {
    unit winner;

    switch (first.type) {
        case KNIGHT:
            switch (second.type) {
                case KNIGHT:
                    winner = make_unit(NONE, -1, -1);
                    break;
                case KING:
                    state.in_game[second.owner] = 0;
                default:
                    winner = first;
                    break;
            }
            break;
        case KING:
            switch (second.type) {
                case KNIGHT:
                    state.in_game[first.owner] = 0;
                    winner = second;
                    break;
                case KING:
                    state.in_game[first.owner] = 0;
                    state.in_game[second.owner] = 0;
                    winner = make_unit(NONE, -1, -1);
                    break;
                default:
                    winner = first;
                    break;
            }
            break;
        case PEASANT:
            switch (second.type) {
                case NONE:
                    winner = first;
                    break;
                case PEASANT:
                    winner = make_unit(NONE, -1, -1);
                    break;
                default:
                    winner = second;
                    break;
            }
            break;
        default:
            assert(false);
            break;
    }

    return winner;
}

/**
 * Checks, if game has ended.
 * @return Appropriate message.
 */
message check_victory () {
    if (state.in_game[1] == false) {
        if (state.in_game[2] == false)
            return DRAW;
        else
            return PLAYER2;
    } else {
        if (state.in_game[2] == false)
            return PLAYER1;
        else
            return CONTINUE;
    }
}

/**
 * Checks if init is done and size correct.
 * @param[in] x1 Number of column, first position.
 * @param[in] y1 Number of row, first position.
 * @param[in] x2 Number of column, second position.
 * @param[in] y2 Number of row, second position.
 * @return True if correct.
 */
bool ready_check (int x1, int y1, int x2, int y2) {
    if (!state.init_done)
        return false;

    if (!size_check(x1, y1, x2, y2, false))
        return false;

    return true;
}

/**
 * Checks if init is units are correct.
 * @param[in] first First unit to check.
 * @param[in] second Second unit to check.
 * @param[in] produce Determines, if command to execute is PRODUCE_.
 * @return True if correct.
 */
bool unit_check (unit *first, unit *second, bool produce) {

    if (first == NULL)
        return false;

    int since_action = state.current_turn - first->last_updated;

    if (produce)
        return (first->type == PEASANT && second == NULL && since_action > 2);

    bool second_right = (second == NULL ? true : second->owner != state.current_player);

    return (second_right && since_action > 0);
}

message move(int x1, int y1, int x2, int y2) {
    if (!ready_check(x1, y1, x2, y2))
        return ERROR;

    unit *first_unit = get_unit(state.units, x1, y1);
    unit *second_unit = get_unit(state.units, x2, y2);

    if (!unit_check(first_unit, second_unit, false))
        return ERROR;

    first_unit->last_updated = state.current_turn;

    if (second_unit == NULL) {
        bool b1 = put_unit(&(state.units), x2, y2, *first_unit);
        assert(b1);
    } else {
        unit winner = fight(*first_unit, *second_unit);
        if (winner.type == NONE) {
            bool b2 = remove_unit(&(state.units), x2, y2);
            assert(b2);
        } else {
            *second_unit = winner;
        }
    }

    bool b3 = remove_unit(&(state.units), x1, y1);
    assert(b3);

    return check_victory();
}

/**
 * Produces unit of given type, at given position.
 * @param[in] type Unit type.
 * @param[in] x1 Number of column, first position.
 * @param[in] y1 Number of row, first position.
 * @param[in] x2 Number of column, second position.
 * @param[in] y2 Number of row, second position.
 * @return Appropriate message.
 */
message produce (unit_type type, int x1, int y1, int x2, int y2) {
    if (type != KNIGHT && type != PEASANT)
        return ERROR;

    if (!ready_check(x1, y1, x2, y2))
        return ERROR;

    unit *first_unit = get_unit(state.units, x1, y1);
    unit *second_unit = get_unit(state.units, x2, y2);

    if (!unit_check(first_unit, second_unit, true))
        return ERROR;

    first_unit->last_updated = state.current_turn;

    unit to_put = make_unit(type, state.current_player, state.current_turn - 1);

    bool b1 = put_unit(&(state.units), x2, y2, to_put);
    assert(b1);

    return CONTINUE;
}

message produce_knight(int x1, int y1, int x2, int y2) {
    return produce(KNIGHT, x1, y1, x2, y2);
}

message produce_peasant(int x1, int y1, int x2, int y2) {
    return produce(PEASANT, x1, y1, x2, y2);
}

message end_turn() {
    if (!state.init_done)
        return ERROR;

    state.current_player++;

    if (state.current_player > state.number_of_players) {
        state.current_turn++;
        state.current_player = 1;
    }

    if (state.current_turn > state.turns) {
        assert(check_victory() == CONTINUE);
        return DRAW;
    }

    return CONTINUE;
}