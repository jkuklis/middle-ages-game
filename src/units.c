/** @file
    Implementation of units module.

 */

#include "units.h"

unit make_unit (unit_type type, int owner, int turn) {
    unit to_return;
    to_return.type = type;
    to_return.owner = owner;
    to_return.last_updated = turn;
    return to_return;
}

unit mock_unit () {
    unit to_return;
    to_return.type = MOCK;
    return to_return;
}

