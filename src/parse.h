/** @file
    Interface of parser.

 */

#ifndef PARSE_H
#define PARSE_H

/**
 * Enumerated type for specifying command name.
 */
typedef enum def_command_name {
    NO_NAME_YET, /**< Not parsed yet. */
    INIT, /**< Initialize a game. */
    MOVE, /**< Move specific unit. */
    PRODUCE_KNIGHT, /**< Produce a knight. */
    PRODUCE_PEASANT, /**< Produce a peasant. */
    END_TURN, /**< End current player's turn. */
    WRONG_COMMAND /**< Parsing error. */
} command_name;

/**
 * Structure for holding command data.
 */
typedef struct def_command {
    command_name name; /**< Name of command. */
    int size; /**< Size of board. */
    int turns; /**< Number of turns before game's end. */
    int player; /**< Player to act. */
    int x1; /**< Column number of first position. */
    int y1; /**< Row number of first position. */
    int x2; /**< Column number of second position. */
    int y2; /**< Row number of second position. */
} command;

/** Reads a command. Sets specific values to *result.
  */
void parse_command(command *parser);

#endif /* PARSE_H */
