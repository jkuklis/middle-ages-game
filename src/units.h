/** @file
    Interface of units module.

 */

#ifndef UNITS_H
#define UNITS_H

/**
 * Enumerated type for specifying unit's type.
 */
typedef enum def_unit_type {
    MOCK, /**< Not real, used for searching. */
    NONE, /**< Unoccupied position. */
    KING, /**< King. */
    KNIGHT, /**< Knight. */
    PEASANT /**< Peasant. */
} unit_type;

/**
 * Structure for holding unit data.
 */
typedef struct def_unit {
    unit_type type; /**< Unit type. */
    int owner; /**< Unit's owner. */
    int last_updated; /**< Turn of last action. */
} unit;

/**
 * Creates unit of specified characteristics.
 * @param[in] type Type of created unit.
 * @param[in] owner Owner of created unit.
 * @param[in] turn Turn of unit's last action.
 * @return Unit that matches given characteristics.
 */
unit make_unit (unit_type type, int owner, int turn);

/**
 * Creates mock unit for search purposes.
 * Used only in container.c.
 * @return Mock unit.
 */
unit mock_unit ();

#endif //MIDDLE_AGES_UNITS_H
