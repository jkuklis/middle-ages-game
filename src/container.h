/** @file
    Interface of container.

 */

#include <stdbool.h>
#include "units.h"

#ifndef CONTAINER_H
#define CONTAINER_H

/**
 * Structure for holding container data.
 */
typedef struct def_container {
    void *tree; /**< Actual container. */
} container;

/**
 * Frees the container.
 * All the tree references and referenced data are freed.
 */
void free_container(container *to_free);

/**
 * Puts a unit at given position.
 * @param[in] to Reference to a container.
 * @param[in] x Column number of the position.
 * @param[in] y Row number of the position.
 * @param[in] to_put Unit that is to be put.
 * @return True if operation is successful, false otherwise.
 */
bool put_unit (container *to, int x, int y, unit to_put);

/**
 * Reads a unit at given position.
 * @param[in] from Container.
 * @param[in] x Column number of the position.
 * @param[in] y Row number of the position.
 * @return Unit at the position, if found, otherwise None-type unit.
 */
unit read_unit (container from, int x, int y);

/**
 * Gets a pointer to unit at given position.
 * @param[in] from Container.
 * @param[in] x Column number of the position.
 * @param[in] y Row number of the position.
 * @return Pointer to unit at the position, if found, otherwise NULL.
 */
unit *get_unit (container from, int x, int y);

/**
 * Removes unit from given position.
 * @param[in] from Container.
 * @param[in] x Column number of the position.
 * @param[in] y Row number of the position.
 * @return True if operation is successful, false otherwise.
 */
bool remove_unit (container *from, int x, int y);

#endif /* CONTAINER_H */