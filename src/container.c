/** @file
    Implementation of container.

 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define __USE_GNU 1
#include <search.h>

#include "container.h"

/**
 * Structure for holding data in a tree.
 */
typedef struct def_node {
    int x; /**< Column number. */
    int y; /**< Row number. */
    unit *unit_at; /**< Unit at the position */
} node;

/**
 * Creates a node pointer.
 * @param[in] x Column number.
 * @param[in] y Row number.
 * @param[in] unit_at Unit at the position
 * @return Node pointer with given characteristics.
 */
node *make_node (int x, int y, unit unit_at) {
    node *to_return = malloc(sizeof(node));
    to_return->x = x;
    to_return->y = y;
    to_return->unit_at = malloc(sizeof(node));
    *(to_return->unit_at) = unit_at;
    return to_return;
}

/**
 * Compares two nodes in a tree.
 * @param[in] left Pointer to left son in a tree.
 * @param[in] right Pointer to right son in a tree.
 * @return 1, if left > right, -1, if left < right, otherwise 0.
 */
int node_compare (const void* left, const void* right) {
    const node *l_node = left;
    const node *r_node = right;

    if (l_node->x < r_node->x) {
        return -1;
    } else if (l_node->x > r_node->x) {
        return 1;
    } else {
        if (l_node->y < r_node->y) {
            return -1;
        } else if (l_node->y > r_node->y) {
            return 1;
        } else {
            return 0;
        }
    }
}

/**
 * Frees data safely.
 * @param[in] data Data to free.
 */
void free_data (void *data) {
    node *to_free = data;
    free(to_free->unit_at);
    free(data);
}

void free_container (container *to_free) {
    tdestroy(to_free->tree, free_data);
}

bool put_unit (container *to, int x, int y, unit to_put) {
    node *node_to_put = make_node(x, y, to_put);

    node *effect = tsearch(node_to_put, &(to->tree), node_compare);

    assert(effect != NULL);

    node *found = *(node **)effect;

    if (found != node_to_put) {
        free_data(node_to_put);
        return false;
    }

    return true;
}

unit read_unit (container from, int x, int y) {
    node *to_find = make_node(x ,y , mock_unit());

    node *effect = tfind(to_find, &(from.tree), node_compare);

    free_data(to_find);

    if (effect == NULL)
        return make_unit(NONE, -1, -1);

    node *data = *(node **)effect;

    return *(data->unit_at);
}

unit *get_unit (container from, int x, int y) {
    node *to_find = make_node(x, y, mock_unit());

    node *effect = tfind(to_find, &(from.tree), node_compare);

    free_data(to_find);

    if (effect == NULL)
        return NULL;

    node *data = *(node **)effect;

    return data->unit_at;
}

bool remove_unit (container *to, int x, int y) {
    node *to_find = make_node(x, y, mock_unit());

    node *effect = tfind(to_find, &(to->tree), node_compare);

    if (effect == NULL) {
        free_data(to_find);
        return false;
    }
    node *to_free = *(node **)effect;

    tdelete(to_find, &(to->tree), node_compare);

    free_data(to_find);
    free_data(to_free);

    return true;
}