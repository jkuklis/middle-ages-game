/** @file
    Interface of game engine.

 */

#ifndef ENGINE_H
#define ENGINE_H

/**
 * Enumerated type for passing information about game state.
 */
typedef enum def_message {
    ERROR, /**< Execution error. */
    CONTINUE, /**< Continue. */
    DRAW, /**< Draw. */
    PLAYER1, /**< First player wins. */
    PLAYER2 /**< Second player wins. */
} message;

/**
 * Initializes a game. Needed before first INIT.
 */
void start_game();

/**
 * Frees memory. Needed after finishing game.
 */
void end_game();

/**
 * Initializes a game with size of a board, number of rounds and positions of kings.
 * Two init calls are needed to start a game.
 * @param[in] size Amount of board columns, rows.
 * @param[in] turns Amount of turns before game end.
 * @param[in] player Number of player.
 * @param[in] x1 Column number of first player's king position.
 * @param[in] y1 Row number of first player's king position.
 * @param[in] x2 Column number of second player's king position.
 * @param[in] y2 Row number of second player's king position.
 * @return CONTINUE or ERROR.
 */
message init(int size, int turns, int player, int x1, int y1, int x2, int y2);

/**
 * Makes a move.
 * @param[in] x1 Column number before a move.
 * @param[in] y1 Row number before a move.
 * @param[in] x2 Column number after a move.
 * @param[in] y2 Row number before a move.
 * @return Message which depends on game state.
 */
message move(int x1, int y1, int x2, int y2);

/**
 * Produces a knight.
 * @param[in] x1 Column number of a peasant.
 * @param[in] y1 Row number of a peasant.
 * @param[in] x2 Destination column number.
 * @param[in] y2 Destination row number.
 * @return CONTINUE or ERROR
 */
message produce_knight(int x1, int y1, int x2, int y2);

/**
 * Produces a peasant.
 * @param[in] x1 Column number of a peasant.
 * @param[in] y1 Row number of a peasant.
 * @param[in] x2 Destination column number.
 * @param[in] y2 Destination row number.
 * @return CONTINUE or ERROR
 */
message produce_peasant(int x1, int y1, int x2, int y2);

/**
 * Ends current player's turn.
 * @return CONTINUE, DRAW or ERROR
 */
message end_turn();

/**
 * Prints (into stdout) top-left corner of the board of size m x m where m = min(n,10).
 */
void print_topleft();

#endif /* ENGINE_H */
