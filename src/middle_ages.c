/** @file
    Implementation of game.

 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#include "parse.h"
#include "engine.h"

/**
 * Executes command.
 * @param[in] to_execute Command to be executed.
 * @return Appropriate message.
 */
message execute_command (command *to_execute) {

    int name = to_execute->name;
    int size = to_execute->size;
    int turns = to_execute->turns;
    int player = to_execute->player;
    int x1 = to_execute->x1;
    int y1 = to_execute->y1;
    int x2 = to_execute->x2;
    int y2 = to_execute->y2;

    message answer;

    switch (name) {
        case INIT:
            answer = init(size, turns, player, x1, y1, x2, y2);
            break;
        case MOVE:
            answer = move(x1, y1, x2, y2);
            break;
        case PRODUCE_KNIGHT:
            answer = produce_knight(x1, y1, x2, y2);
            break;
        case PRODUCE_PEASANT:
            answer = produce_peasant(x1, y1, x2, y2);
            break;
        case END_TURN:
            answer = end_turn();
            break;
        default:
            assert(false);
            break;
    }

    if (name != END_TURN && answer != ERROR)
        print_topleft();

    return answer;
}

/**
 * Handles execution messages.
 * @param[in] to_handle Message to handle.
 */
void handle (message to_handle) {
    switch (to_handle) {
        case CONTINUE:
            break;
        case DRAW:
            fputs("draw\n", stderr);
            break;
        case PLAYER1:
            fputs("player 1 won\n", stderr);
            break;
        case PLAYER2:
            fputs("player 2 won\n", stderr);
            break;
        default:
            assert(false);
            break;
    }
}

/**
 * Frees memory and ends game.
 * @param[in] to_free Command to free.
 * @param[in] error Determines, if an error occurred.
 */
void close (command *to_free, bool error) {
    if (error)
        fputs("input error\n", stderr);
    end_game();
    free(to_free);
}

/**
 * The Game.
 */
int main() {

    start_game();

    command *parsing_data;
    parsing_data = malloc(sizeof(command));

    while (1) {

        parse_command(parsing_data);

        if (parsing_data->name == WRONG_COMMAND) {
            close(parsing_data, true);
            return 42;
        }

        message answer = execute_command(parsing_data);

        if (answer == ERROR) {
            close(parsing_data, true);
            return 42;
        }

        handle (answer);

        if (answer != CONTINUE) {
            break;
        }
    }

    close(parsing_data, false);

    return 0;
}