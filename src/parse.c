/** @file
    Implementation of parser.

 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#include "parse.h"

#define MAX_LINE_LENGTH 101
#define MAX_INT 0x7FFFFFFF

/**
 * Initializes the values of *result.
 * @param[in] result Pointer to initialize.
 */
void initialize (command *parser) {
    parser->name = NO_NAME_YET;
    parser->size = parser->turns = parser->player = 0;
    parser->x1 = parser->y1 = parser->x2 = parser->y2 = 0;
}

/**
 * Parses command name.
 * Reads name from buffer and checks its correctness, then sets value of parser.
 * @param[in] buffer Buffer to read.
 * @param[in] parser Command data.
 * @param[in] index Index to first place after name and a single char.
 */
void get_command_name (char *buffer, command *parser, size_t *index) {

    char *name = strtok(buffer, " ");
    *index = strlen(name) + 1;

    if (strcmp(name, "END_TURN\n") == 0) {
        parser->name = END_TURN;
    } else if (strcmp(name, "INIT") == 0) {
        parser->name = INIT;
    } else if (strcmp(name, "MOVE") == 0) {
        parser->name = MOVE;
    } else if (strcmp(name, "PRODUCE_KNIGHT") == 0) {
        parser->name = PRODUCE_KNIGHT;
    } else if (strcmp(name, "PRODUCE_PEASANT") == 0) {
        parser->name = PRODUCE_PEASANT;
    } else {
        parser->name = WRONG_COMMAND;
    }

    if (parser->name != END_TURN && parser->name != WRONG_COMMAND && !isdigit(*(buffer + *index)))
        parser->name = WRONG_COMMAND;
}

/**
 * Checks if number is within range.
 * @param[in] to_check Number to check.
 * @param[in] lower Lower bound.
 * @param[in] upper Upper bound.
 * @return True if within range, false otherwise.
 */
bool range_check (long long to_check, int lower, int upper) {
    return !(to_check < lower || to_check > upper);
}

/**
 * Checks char correctness.
 * @param[in] auxiliary String to check.
 * @param[in] last Determines if token is the last one to parse.
 * @return True if correct.
 */
bool char_check (char *auxiliary, bool last) {
    if (last)
        return (*auxiliary == '\n');
    else
        return (*auxiliary == ' ' && isdigit(*(auxiliary + 1)));
}

/**
 * Parse next number.
 * Checks range and char correctness.
 * @param[in] parser Command data.
 * @param[in] auxiliary String to read.
 * @param[in] lower Lower bound of range.
 * @param[in] upper Upper bound of range.
 * @param[in] last Determines if token is the last one to parse.
 * @return Number read.
 */
long long get_next_number (command *parser, char **auxiliary, int lower, int upper, bool last) {
    if (parser->name == WRONG_COMMAND)
        return 0;

    long long temp = strtoll(*auxiliary, auxiliary, 10);

    if (range_check(temp, lower, upper) && char_check(*auxiliary, last)) {
        return temp;
    } else {
        parser->name = WRONG_COMMAND;
        return -1;
    }
}

/**
 * Parses data exclusive for init.
 * @param[in] parser Command data.
 * @param[in] auxiliary String to read.
 */
void parse_init (command *parser, char **auxiliary) {
    parser->size = (int)get_next_number(parser, auxiliary, 9, MAX_INT, false);
    parser->turns = (int)get_next_number(parser, auxiliary, 1, MAX_INT, false);
    parser->player = (int)get_next_number(parser, auxiliary, 1, 2, false);
}

/**
 * Parses position and checks correctness.
 * @param[in] parser Command data.
 * @param[in] auxiliary String to read.
 */
void parse_position (command *parser, char **auxiliary) {
    parser->x1 = (int)get_next_number(parser, auxiliary, 1, MAX_INT, false);
    parser->y1 = (int)get_next_number(parser, auxiliary, 1, MAX_INT, false);
    parser->x2 = (int)get_next_number(parser, auxiliary, 1, MAX_INT, false);
    parser->y2 = (int)get_next_number(parser, auxiliary, 1, MAX_INT, true);

    if (parser->name == INIT) {
        if (abs(parser->x1 - parser->x2) < 8 && abs(parser->y1 - parser->y2) < 8)
            parser->name = WRONG_COMMAND;

    } else if (parser->name != WRONG_COMMAND){
        if (parser->x1 == parser->x2 && parser->y1 == parser->y2)
            parser->name = WRONG_COMMAND;
        if (abs(parser->x1 - parser->x2) > 1 || abs(parser->y1 - parser->y2) > 1 )
            parser->name = WRONG_COMMAND;
    }

}

void parse_command (command *parser) {
    initialize(parser);

    char buffer[MAX_LINE_LENGTH];
    fgets(buffer, MAX_LINE_LENGTH, stdin);

    // index of first character after first whitespace in buffer
    // may be out of range
    size_t index;

    get_command_name(buffer, parser, &index);

    command_name name = parser->name;
    char *auxiliary = buffer + index;

    if (name == INIT)
        parse_init(parser, &auxiliary);

    if (name == INIT || name == MOVE || name == PRODUCE_KNIGHT || name == PRODUCE_PEASANT)
        parse_position(parser, &auxiliary);

}
